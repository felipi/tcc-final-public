#pragma once
#include "BaseCharacter.h"

class HeroCharacter : public BaseCharacter {
public:
	HeroCharacter(void);
	~HeroCharacter(void);

	bool initWithSprite(CCString*);
	void draw();
	void DrawHealthBar();
};

