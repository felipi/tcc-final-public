#include "BattleLayer.h"
#include "BaseCharacter.h"
#include "HeroCharacter.h"
#include "EnemyCharacter.h"

using namespace cocos2d;

bool BattleLayer::passability[kMapWidth][kMapHeight];
bool BattleLayer::playerMovement[4] = {0,0,0,0};
BattleLayer* BattleLayer::layer;
CCDictionary* BattleLayer::spritesheets;
CCLabelTTF* BattleLayer::label;

bool BattleLayer::PassableAt(int x, int y){
	return passability[x][y];
}

HeroCharacter* BattleLayer::Hero(){
	return layer->hero;
}

float BattleLayer::Level(){
	return layer->level;
}

float BattleLayer::Score(){
	return layer->score;
}

void BattleLayer::AddScore(float s){
	layer->score += s;
	layer->level = 1 + ((int)layer->score/50);
}

CCArray* BattleLayer::SpawnPoints(){
	return layer->spawnPoints;
}

CCArray* BattleLayer::Enemies(){
	return layer->enemies;
}

CCSpriteBatchNode* BattleLayer::Spritesheet(CCString* name, bool addInstantly){
	CCSpriteBatchNode* sheet;
	if(spritesheets->objectForKey(name->m_sString) == NULL){
		sheet = CCSpriteBatchNode::batchNodeWithFile(name->getCString(), 100);
		spritesheets->setObject(sheet, name->m_sString);
		if(addInstantly){
			BattleLayer::layer->addChild(sheet);
		}
	}else{
		sheet = (CCSpriteBatchNode*)spritesheets->valueForKey(name->m_sString);
	}
	return sheet;
}

CCScene* BattleLayer::scene()
{
    CCScene * scene = NULL;
    do 
    {
        // 'scene' is an autorelease object
        scene = CCScene::node();
        CC_BREAK_IF(! scene);

		// 'layer' is an autorelease object
        layer = BattleLayer::node();
        CC_BREAK_IF(! layer);

        // add layer as a child to scene
        scene->addChild(layer);
    } while (0);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool BattleLayer::init()
{
    bool bRet = false;
    do 
    {
        //////////////////////////////////////////////////////////////////////////
        // super init first
        //////////////////////////////////////////////////////////////////////////

        CC_BREAK_IF(! CCLayer::init());

        //////////////////////////////////////////////////////////////////////////
        // add your codes below...
        //////////////////////////////////////////////////////////////////////////
		audio.Init();
		emotionController.Init(ccs("model.plist"));
		emotionController.registerEmotion(ccs("hp"), 3000, 0, 3000);
		emotionController.registerEmotion(ccs("enemies"), 0, 0, 12);
		emotionController.registerEmotion(ccs("level"), 1, 1, 5);

		spritesheets = new CCDictionary();
		enemies = new CCArray();
		spawnPoints = new CCArray();

		Spritesheet(ccs("hero.png"));
		Spritesheet(ccs("enemy1.png"));

		level = 1;
		score = 0;

		//isKeyboadEnabled = true;
		label = CCLabelTTF::labelWithString("Score: ", "Marker Felt", 18);
		CCSize size = CCDirector::sharedDirector()->getWinSize();
		label->setPosition(ccp(size.width - 100, size.height - 10));

		hero = new HeroCharacter();
		hero->initWithSprite(ccs("hero.png"));
		hero->autorelease();

		CCTMXTiledMap* tilemap = CCTMXTiledMap::tiledMapWithTMXFile("level1.tmx");
		this->passabilityFor(tilemap);
		CCTMXObjectGroup* objects = tilemap->objectGroupNamed("Objects");
		CCDictionary* dict = objects->objectNamed("HeroPosition");
		int herox = dict->valueForKey(ccs("x")->m_sString)->intValue();
		int heroy = dict->valueForKey(ccs("y")->m_sString)->intValue();
		hero->setPosition(ccp(herox, heroy));

		for(int o=0; o < objects->getObjects()->count(); o++){
			CCDictionary* dict = (CCDictionary*)objects->getObjects()->objectAtIndex(o);
			CCString* type = (CCString*)dict->valueForKey(ccs("type")->m_sString);
			if(type->m_sString == "enemy" ){
				CCDictionary* object = dict;
				CCString* spawnx = (CCString*)object->valueForKey(ccs("x")->m_sString);
				CCString* spawny = (CCString*)object->valueForKey(ccs("y")->m_sString);
				CCArray* spawn = CCArray::arrayWithObjects(spawnx, spawny, NULL);
				spawnPoints->addObject(spawn);
			}
		}

		this->addChild(label);
		this->addChild(tilemap, -100);
		this->addChild(hero, 0);

		CCArray* s = spritesheets->allKeys();
		for(int c = 0; c < s->count(); c++){
			CCString* key = (CCString*)s->objectAtIndex(c);
			this->addChild((CCSpriteBatchNode*)spritesheets->objectForKey( key->m_sString ) );
		}
		schedule(schedule_selector(BattleLayer::Loop));

        bRet = true;
    } while (0);

    return bRet;
}

void BattleLayer::AddEnemyAtPoint(CCArray* point){
	EnemyCharacter* enemy = new EnemyCharacter();
	enemy->initWithSprite(ccs("enemy1.png"));
	enemy->autorelease();
	CCFloat enemyx = ((CCString*)point->objectAtIndex(0))->intValue();
	CCFloat enemyy = ((CCString*)point->objectAtIndex(1))->intValue();
	enemy->setPosition(ccp(enemyx, enemyy));
	this->addChild(enemy, 5);
	enemies->addObject(enemy);	
}


void BattleLayer::passabilityFor(CCTMXTiledMap* tilemap){
	CCTMXLayer *layer = tilemap->layerNamed("Background");
	for(int x=0; x<tilemap->getMapSize().width; x++){
		for(int y=0; y<tilemap->getMapSize().height; y++){
			int gid= layer->tileGIDAt(ccp(x,y));
			CCDictionary* dict = tilemap->propertiesForGID(gid);
			bool solid = false;
			if(dict != NULL){
			CCArray* keys = dict->allKeys();
			for(int k=0; k < keys->count(); k++){
				if( (CCString*)keys->objectAtIndex(k) == ccs("solid"))
					solid = true;
			}
			}
			if(solid){
				passability[x][y] = false;
			}else {
				passability[x][y] = true;
			}

		}
	}
}

void BattleLayer::Loop(ccTime dt){
	static float time = 0;
	static float counter = 15;
	time -= dt;
	emotionController.updateEmotion(ccs("hp"), Hero()->life);
	emotionController.updateEmotion(ccs("level"), level);
	emotionController.updateEmotion(ccs("enemies"), enemies->count());
	audio.UpdateComponents(emotionController.getMedianForPoints().x, emotionController.getMedianForPoints().y);
	audio.Update();

	CCString* labelString = CCString::stringWithFormat("Pontos: %.0f Nivel: %.0f", Score(), Level());
	label->setString(labelString->getCString());

	// Up Arrow or W
	if (playerMovement[0]) {
		Hero()->vely = 1;
	}else// Down Arrow or S
	if (playerMovement[1]) {
		Hero()->vely = -1;
	}else {
		Hero()->vely = 0;
	}

	// left Arrow or D
	if (playerMovement[2]) {
		Hero()->velx = -1;
	}else
	// Right Arrow or A
	if (playerMovement[3]) {
		Hero()->velx = 1;
	}else {
		Hero()->velx = 0;
	}

	if(time<0){
		std::cout << "Valencia = " << emotionController.getMedianForPoints().x << std::endl;
		std::cout << "Ativacao = " << emotionController.getMedianForPoints().y << std::endl;

		for(int s=0; s < SpawnPoints()->count(); s++){
			this->AddEnemyAtPoint((CCArray*)SpawnPoints()->objectAtIndex(s));
		}
		
		time=counter - (Level()/10);
	}
}

void BattleLayer::onExit(){
	audio.Destroy();
}