#include "HeroCharacter.h"

HeroCharacter::HeroCharacter(void)
{
}


HeroCharacter::~HeroCharacter(void)
{
}

bool HeroCharacter::initWithSprite(CCString* file){
	bool bRet = false;
    do 
    {
        //////////////////////////////////////////////////////////////////////////
        // super init first
        //////////////////////////////////////////////////////////////////////////

		CC_BREAK_IF(! BaseCharacter::initWithSprite(file));

		//Code

		speed = 9;
		life = 3000;

	}while(0);

	return bRet;
}

void HeroCharacter::draw(){
	DrawHealthBar();

	BaseCharacter::draw();
}

void HeroCharacter::DrawHealthBar(){
	if(life<=0)return;
	CCRect rect = CCRect(-(kCharWidth/2), kCharHeight/2, kCharWidth, 5);
	CCPoint vertices[4]={
        ccp(rect.origin.x,rect.origin.y),
        ccp(rect.origin.x+rect.size.width,rect.origin.y),
        ccp(rect.origin.x+rect.size.width,rect.origin.y+rect.size.height),
        ccp(rect.origin.x,rect.origin.y+rect.size.height),
    };
	
	rect = CCRect(-(kCharWidth/2)+1, (kCharHeight/2)+1, (kCharWidth-2)*life/3000, 3);
	CCPoint verticesLife[4]={
        ccp(rect.origin.x,rect.origin.y),
        ccp(rect.origin.x+rect.size.width,rect.origin.y),
        ccp(rect.origin.x+rect.size.width,rect.origin.y+rect.size.height),
        ccp(rect.origin.x,rect.origin.y+rect.size.height),
    };
	
	glDisable(GL_TEXTURE_2D);
	//glDisableClientState(GL_COLOR_ARRAY);
	//glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glLineWidth(1.0f);
	ccc4f(0,0,0,1);
	//glColor4f(0, 0, 0, 1.0);
    //ccDrawPoly(vertices, 4, true);
	if(life > 0){
		ccc4f(0,0,1,1);
		//glColor4f(0, 0.0, 1.0, 1.0);
		ccDrawPoly(verticesLife, 4, true);
	}
	glEnable(GL_TEXTURE_2D);
	//glEnableClientState(GL_COLOR_ARRAY);
	//glEnableClientState(GL_TEXTURE_COORD_ARRAY);
}