#pragma once
#ifndef __BATTLELAYER_SCENE_H__
#define __BATTLELAYER_SCENE_H__

#include "cocos2d.h"
#include "HeroCharacter.h"
#include "Audio.h"
#include "EmotionController.h"

using namespace cocos2d;

#define kMapWidth 45
#define kMapHeight 30
#define kTileWidth 16
#define kTileHeight 16

class BattleLayer :	public cocos2d::CCLayer
{
private:
	HeroCharacter* hero;
	CCArray* enemies;
	CCArray* spawnPoints;
	float score, level;

public:	
	Audio audio;
	EmotionController emotionController;
	static bool passability[kMapWidth][kMapHeight];
	static bool playerMovement[4];
	static CCDictionary* spritesheets;
	static BattleLayer* layer;
	static CCLabelTTF* label;

	static HeroCharacter* Hero();
	static float Level();
	static float Score();
	static CCArray* SpawnPoints();
	static void AddScore(float);
	static CCArray* Enemies();
	static bool PassableAt(int, int);
	static CCSpriteBatchNode* Spritesheet(CCString*, bool addInstantly = false);
	void onExit();
	void passabilityFor(CCTMXTiledMap*);
	void Loop(ccTime);
	void AddEnemyAtPoint(CCArray*);
	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::CCScene* scene();

    // implement the "static node()" method manually
    LAYER_NODE_FUNC(BattleLayer);
};

#endif