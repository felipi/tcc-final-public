#include "EnemyCharacter.h"
#include "HeroCharacter.h"
#include "BattleLayer.h"

EnemyCharacter::EnemyCharacter(void)
{
}


EnemyCharacter::~EnemyCharacter(void)
{
}

bool EnemyCharacter::initWithSprite(CCString* file){
	bool bRet = false;
    do 
    {
        //////////////////////////////////////////////////////////////////////////
        // super init first
        //////////////////////////////////////////////////////////////////////////

		CC_BREAK_IF(! BaseCharacter::initWithSprite(file));

		//Code
		strength = 12 + (1.4 * BattleLayer::Level());
		speed = 4 + (0.4 * BattleLayer::Level());
		schedule(schedule_selector(EnemyCharacter::ChaseHero));

	}while(0);

	return bRet;
}

void EnemyCharacter::ChaseHero(ccTime dt){
	HeroCharacter* hero = BattleLayer::Hero();

	if (hero->getPosition().x > this->getPosition().x) {
		velx = 1;
	}else if (hero->getPosition().x < this->getPosition().x) {
		velx = -1;
	}
	
	if (hero->getPosition().y > this->getPosition().y) {
		vely = 1;
	}else if (hero->getPosition().y < this->getPosition().y) {
		vely = -1;
	}
}

void EnemyCharacter::draw(){
	DrawHealthBar();

	BaseCharacter::draw();
}

void EnemyCharacter::DrawHealthBar(){
	if(life<=0) return;
	CCRect rect = CCRect(-(kCharWidth/2), kCharHeight/2, kCharWidth, 5);
	CCPoint vertices[4]={
        ccp(rect.origin.x,rect.origin.y),
        ccp(rect.origin.x+rect.size.width,rect.origin.y),
        ccp(rect.origin.x+rect.size.width,rect.origin.y+rect.size.height),
        ccp(rect.origin.x,rect.origin.y+rect.size.height),
    };

	rect = CCRect(-(kCharWidth/2)+1, (kCharHeight/2)+1, (kCharWidth-2)*life/100, 3);
	CCPoint verticesLife[4]={
        ccp(rect.origin.x,rect.origin.y),
        ccp(rect.origin.x+rect.size.width,rect.origin.y),
        ccp(rect.origin.x+rect.size.width,rect.origin.y+rect.size.height),
        ccp(rect.origin.x,rect.origin.y+rect.size.height),
    };

	glDisable(GL_TEXTURE_2D);
	//glDisableClientState(GL_COLOR_ARRAY);
	//glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glLineWidth(1.0f);	
	//glColor4f(0, 0, 0, 1.0);
    //ccDrawPoly(vertices, 4, true);
	if(life >0){
		//glColor4f(0, 1.0, 0, 1.0);
		ccDrawPoly(verticesLife, 4, true);
	}
	glEnable(GL_TEXTURE_2D);
	//glEnableClientState(GL_COLOR_ARRAY);
	//glEnableClientState(GL_TEXTURE_COORD_ARRAY);
}
