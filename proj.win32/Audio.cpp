#include "Audio.h"

const float UPDATE_INTERVAL = 100.0f;

void Audio::ERRCHECK(FMOD_RESULT result){
    if (result != FMOD_OK)
    {
        printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
        exit(-1);
    }
}

int                   key;
float* fmodp_speed, fmodp_time;

void Audio::Init(){
    ERRCHECK(result = FMOD::EventSystem_Create(&eventsystem));
	
    ERRCHECK(result = eventsystem->init(64, FMOD_INIT_NORMAL, 0, FMOD_EVENT_INIT_NORMAL));
	ERRCHECK(result = eventsystem->setMediaPath("Media/"));
    ERRCHECK(result = eventsystem->load("tcc.fev", 0, 0));
	//ERRCHECK(result = fmodsystem->createSound("pepe.fsb", FMOD_CREATESTREAM, 0, &sound));
	//ERRCHECK(result = eventsystem->preloadFSB("pepe.fsb", 0, sound));
	ERRCHECK(result = eventsystem->getMusicSystem(&musicsystem));
	ERRCHECK(result = musicsystem->loadSoundData(FMOD_EVENT_RESOURCE_SAMPLES, FMOD_EVENT_NONBLOCKING));	
	
	FMOD_MUSIC_ITERATOR mit;
	
	ERRCHECK(result = musicsystem->getParameters(&mit, "valence"));
	ERRCHECK(result = musicsystem->setParameterValue(mit.value->id, componentx));

	ERRCHECK(result = musicsystem->getParameters(&mit, "arousal"));
	ERRCHECK(result = musicsystem->setParameterValue(mit.value->id, componenty));

	ERRCHECK(result = musicsystem->setVolume(1.0f));
	
	ERRCHECK(result = musicsystem->getCues(&mit, "Bass"));
	ERRCHECK(result = musicsystem->prepareCue(mit.value->id, &b_prompt));
	ERRCHECK(result = musicsystem->getCues(&mit, "Drums"));
	ERRCHECK(result = musicsystem->prepareCue(mit.value->id, &d_prompt));
	ERRCHECK(result = musicsystem->getCues(&mit, "Melody"));
	ERRCHECK(result = musicsystem->prepareCue(mit.value->id, &m_prompt));
	ERRCHECK(result = musicsystem->getCues(&mit, "Harmony"));
	ERRCHECK(result = musicsystem->prepareCue(mit.value->id, &h_prompt));
	ERRCHECK(result = musicsystem->getCues(&mit, "Percussion"));
	ERRCHECK(result = musicsystem->prepareCue(mit.value->id, &p_prompt));
	ERRCHECK(result = musicsystem->getCues(&mit, "Tubes"));
	ERRCHECK(result = musicsystem->prepareCue(mit.value->id, &t_prompt));

	ERRCHECK(result = b_prompt->begin());
	ERRCHECK(result = d_prompt->begin());
	ERRCHECK(result = m_prompt->begin());
	ERRCHECK(result = h_prompt->begin());
	ERRCHECK(result = p_prompt->begin());
	ERRCHECK(result = t_prompt->begin());
}

void Audio::UpdateComponents(float x, float y){
	componentx = x;
	componenty = y;
}

void Audio::Update(){
	FMOD_MUSIC_ITERATOR mit;
	
	ERRCHECK(result = musicsystem->getParameters(&mit, "valence"));
	ERRCHECK(result = musicsystem->setParameterValue(mit.value->id, componentx));

	ERRCHECK(result = musicsystem->getParameters(&mit, "arousal"));
	ERRCHECK(result = musicsystem->setParameterValue(mit.value->id, componenty));
	
	ERRCHECK(result = eventsystem->update());
}

void Audio::Destroy(){
	
	//ERRCHECK(result = eventgroup->freeEventData());
	ERRCHECK(result = b_prompt->end());
	ERRCHECK(result = b_prompt->release());
	ERRCHECK(result = d_prompt->end());
	ERRCHECK(result = d_prompt->release());
	ERRCHECK(result = h_prompt->end());
	ERRCHECK(result = h_prompt->release());
	ERRCHECK(result = m_prompt->end());
	ERRCHECK(result = m_prompt->release());
	ERRCHECK(result = p_prompt->end());
	ERRCHECK(result = p_prompt->release());
	ERRCHECK(result = t_prompt->end());
	ERRCHECK(result = t_prompt->release());
	ERRCHECK(result = musicsystem->freeSoundData());
    ERRCHECK(result = eventsystem->release());
	
}
//==

Audio::Audio(void){
	componentx = 0.0f;
	componenty = 0.0f;
}


Audio::~Audio(void)
{
}
