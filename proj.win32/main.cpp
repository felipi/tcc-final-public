#include "main.h"
#include "AppDelegate.h"
#include "CCEGLView.h"
#include "BattleLayer.h"
#include <conio.h>
#include <process.h>

USING_NS_CC;

enum
{
  KEY_ESC     = 27,
  KEY_SPACE	  = 32,
  ARROW_UP    = 328,
  ARROW_DOWN  = 336,
  ARROW_LEFT  = 331,
  ARROW_RIGHT = 333
};


void KeyboardInput(void* pMyID){
	int key;
	
	while(true){
		_sleep(1000/30);
		if(kbhit()){
		key = getch();
		if( key == 0 || key == 224 ){
			key = 256 + getch();
		}

		//std::cout << key << std::endl;
		switch(key){
		case KEY_ESC:
			exit(0); break;
		case KEY_SPACE:
			BattleLayer::Hero()->Attack(); break;
		case ARROW_UP:
			BattleLayer::playerMovement[0] = true; break;
		case ARROW_DOWN:
			BattleLayer::playerMovement[1] = true; break;
		case ARROW_LEFT:
			BattleLayer::playerMovement[2] = true; break;
		case ARROW_RIGHT:
			BattleLayer::playerMovement[3] = true; break;
		}
	 }else{
		//std::cout << "KEY UP\n";
		BattleLayer::playerMovement[0] = false;
		BattleLayer::playerMovement[1] = false;
		BattleLayer::playerMovement[2] = false;
		BattleLayer::playerMovement[3] = false;
	}
	}
}

// uncomment below line, open debug console
#define USE_WIN32_CONSOLE

int APIENTRY _tWinMain(HINSTANCE hInstance,
                       HINSTANCE hPrevInstance,
                       LPTSTR    lpCmdLine,
                       int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

#ifdef USE_WIN32_CONSOLE
    AllocConsole();
    freopen("CONIN$", "r", stdin);
    freopen("CONOUT$", "w", stdout);
    freopen("CONOUT$", "w", stderr);
#endif

    // create the application instance
    AppDelegate app;
    CCEGLView& eglView = CCEGLView::sharedOpenGLView();
    eglView.setViewName("Hello World");
    eglView.setFrameSize(720, 480);
    // set the design resolution screen size, if you want to use Design Resoulution scaled to current screen, please uncomment next line.
    // eglView.setDesignResolutionSize(480, 320);

	std::cout << "Controles:\nSetas para mover-se\nEspaco para atacar\nESC para sair\n\n";
	std::cout << "Clique nessa janela e aperte qualquer tecla para iniciar\n";
	getch();

	int threadNbr = 1;
	_beginthread(KeyboardInput, 0, &threadNbr);

    int ret = CCApplication::sharedApplication().run();

#ifdef USE_WIN32_CONSOLE
    FreeConsole();
#endif

    return ret;
}
