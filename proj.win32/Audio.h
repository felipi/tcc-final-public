#pragma once
#include <cstdio>
#include <cstdlib>

//AUDIO-FMOD
#include "fmod_event.hpp"
#include "fmod.hpp"
#include "fmod_errors.h"

class Audio {
public:
	Audio(void);
	~Audio(void);

	FMOD::EventSystem    *eventsystem;
	FMOD::MusicSystem	 *musicsystem;
	FMOD::EventGroup     *eventgroup;
	FMOD::EventCategory  *mastercategory;
	FMOD::Event          *car;
	FMOD::EventParameter *rpm;
	FMOD::EventParameter *load;
	FMOD::MusicPrompt	 *b_prompt;
	FMOD::MusicPrompt	 *m_prompt;
	FMOD::MusicPrompt	 *h_prompt;
	FMOD::MusicPrompt	 *d_prompt;
	FMOD::MusicPrompt	 *p_prompt;
	FMOD::MusicPrompt	 *t_prompt;
	FMOD::Sound			 *sound;
	FMOD::System		 *fmodsystem;
	FMOD_RESULT           result;

	float componentx, componenty;

	void UpdateComponents(float, float);
	void Init();
	void Update();
	void Destroy();
	void ERRCHECK(FMOD_RESULT result);
};

