#pragma once
#include "BaseCharacter.h"

class EnemyCharacter : public BaseCharacter {
public:
	EnemyCharacter(void);
	~EnemyCharacter(void);

	bool EnemyCharacter::initWithSprite(CCString*);
	void DrawHealthBar();
	void ChaseHero(ccTime);
	void draw();
};

