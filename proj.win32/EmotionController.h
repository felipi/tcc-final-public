#pragma once
#include "cocos2d.h"

class EmotionObject {
public:
	EmotionObject(void);
	EmotionObject(float,float,float);
	float minValue, maxValue;
	float currentValue;
	cocos2d::CCPoint minVertex, maxVertex;

	cocos2d::CCPoint getCurrentVertex();
};

class EmotionController{
public:
	EmotionController(void);
	void Init(cocos2d::CCString*);
	~EmotionController(void);

	void updateEmotion(cocos2d::CCString*, float);
	void registerEmotion(cocos2d::CCString*, float, float, float);
	cocos2d::CCPoint getMedianForPoints();

private:
	EmotionObject emotions[3];
	cocos2d::CCArray* models;
};

