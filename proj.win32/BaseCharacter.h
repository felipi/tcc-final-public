#pragma once
#include "cocos2d.h"
#include "cocoa\CCDictionary.h"
#include "cocoa\CCString.h"
#include "cocoa\CCArray.h"

using namespace cocos2d;

#define kCharWidth 24
#define kCharHeight 28
#define kSpriteSize 45
//#define kAttackTime 0.65
#define kDamageTime 0.35

class BaseCharacter :
	public cocos2d::CCSprite
{
public:
	BaseCharacter(void);
	~BaseCharacter(void);

	int direction; //0 down, 1 right, 2 left, 3 up
	int speed;
	float animationStep;
	float kAttackTime;

	CCSpriteBatchNode* spritesheet;
	CCSprite* sprite;
	CCDictionary* states;
	CCString* currentState;

	float velx;
	float vely;

	float life;
	float combo;
	float strength;
	float attackTimer, damageTimer;

	bool isAttacking, isTakingDamage;

	virtual bool initWithSprite(CCString*);
	CCRect getRectFor(int, int);
	void addState(CCString*, int, int, int);
	virtual void Update(ccTime);
	virtual void Attack();
	virtual void ReceiveDamageFrom(BaseCharacter*);
	virtual void removeSpriteFromParent();
};

