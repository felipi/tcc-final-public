#include "EmotionController.h"
using namespace cocos2d;

EmotionObject::EmotionObject(void){}
EmotionObject::EmotionObject(float minv, float maxv, float currentv){
	minValue = minv;
	maxValue = maxv;
	currentValue = currentv;

	minVertex = CCPoint(-1,-1);
	maxVertex = CCPoint(1,1);
}

CCPoint EmotionObject::getCurrentVertex(){
	float percent = ((currentValue-minValue) * 100) / (maxValue-minValue);
	percent /= 100;

	float magnitude = sqrt( pow(maxVertex.x-minVertex.x,2) + pow(maxVertex.y-minVertex.y,2) ); //max magnitude will be 10

	CCPoint differenceVector = CCPoint(maxVertex.x-minVertex.x, maxVertex.y - minVertex.y);
	CCPoint normalizedVector = CCPoint(differenceVector.x/10.0f, differenceVector.y/10.0f);
	CCPoint magnifiedVector = CCPoint(normalizedVector.x * percent, normalizedVector.y * percent);
	CCPoint returnVector = CCPoint(minVertex.x/10.0f + magnifiedVector.x, minVertex.y/10.0f + magnifiedVector.y);
	
	return  returnVector;
}

EmotionController::EmotionController(void){}

void EmotionController::Init(CCString* file){
	models = (CCArray*)CCArray::arrayWithContentsOfFile(file->getCString())->copy();
}


EmotionController::~EmotionController(void)
{
}

void EmotionController::updateEmotion(CCString* name, float newValue){
	for(int i=0; i < models->count(); i++){
		CCDictionary* dict = ((CCDictionary*)models->objectAtIndex(i));
		if(dict->valueForKey(ccs("name")->m_sString)->m_sString == name->m_sString){
			emotions[i].currentValue = newValue;
		}
	}
}

void EmotionController::registerEmotion(CCString* name, float initialValue, float min, float max){
	for(int i=0; i < models->count(); i++){
		CCDictionary* dict = ((CCDictionary*)models->objectAtIndex(i));
		if(dict->valueForKey(ccs("name")->m_sString)->m_sString == name->m_sString){
			emotions[i] = EmotionObject(min, max, initialValue);

			int minx = ((CCString*)dict->objectForKey(ccs("minx")->m_sString))->intValue();
			int miny = ((CCString*)dict->objectForKey(ccs("miny")->m_sString))->intValue();
			int maxx = ((CCString*)dict->objectForKey(ccs("maxx")->m_sString))->intValue();
			int maxy = ((CCString*)dict->objectForKey(ccs("maxy")->m_sString))->intValue();
		
			emotions[i].minVertex = CCPoint(minx, miny);
			emotions[i].maxVertex = CCPoint(maxx, maxy);
		}
	}
}

CCPoint EmotionController::getMedianForPoints(){
	float centerx = (emotions[0].getCurrentVertex().x + emotions[1].getCurrentVertex().x + emotions[2].getCurrentVertex().x)/3.0f;
	float centery = (emotions[0].getCurrentVertex().y + emotions[1].getCurrentVertex().y + emotions[2].getCurrentVertex().y)/3.0f;
	return CCPoint(centerx, centery);
}