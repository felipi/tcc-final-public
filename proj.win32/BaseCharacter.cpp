#include "BaseCharacter.h"
#include "BattleLayer.h"
#include <cmath>

BaseCharacter::BaseCharacter(void)
{
}


BaseCharacter::~BaseCharacter(void)
{
}

bool BaseCharacter::initWithSprite(CCString* file){

	bool bRet = false;
    do 
    {
        //////////////////////////////////////////////////////////////////////////
        // super init first
        //////////////////////////////////////////////////////////////////////////

        CC_BREAK_IF(! CCSprite::init());

        //////////////////////////////////////////////////////////////////////////
        // add your codes below...
        //////////////////////////////////////////////////////////////////////////

		kAttackTime = 0.65;

		direction = 0;
		speed = 4;
		animationStep = 0;
		velx = 0;
		vely = 0;

		life = 100;
		strength = 15;
		combo = 0;

		isAttacking = isTakingDamage = false;

		states = new CCDictionary;

		this->addState(ccs("walk"), 0, 0, 4);
		this->addState(ccs("idle"), 0, 0, 0);
		this->addState(ccs("attack"), 4, 0, 6);
		this->addState(ccs("hit"), 0, 0, 0);
		currentState = ccs("idle");
		spritesheet = BattleLayer::Spritesheet(file, true);
		sprite = CCSprite::spriteWithTexture(spritesheet->getTexture(), getRectFor(0,0));
		spritesheet->addChild(sprite);
       
		schedule(schedule_selector(BaseCharacter::Update));
        bRet = true;
    } while (0);

	return bRet;

}

void BaseCharacter::Update(ccTime dt){
	if(isTakingDamage){
		currentState = ccs("hit");
		damageTimer -= dt;
		velx =0; vely = 0;
		if(damageTimer <= 0){
			isTakingDamage = false;
		}
	}else if(isAttacking){
		currentState = ccs("attack");
		attackTimer -= dt;
		
		if(direction == 1) velx = 1;
		else if(direction == 2) velx = -1;
		else if (direction == 3) vely = 1;
		else if (direction == 0) vely = -1;

		if(attackTimer <= 0){
			isAttacking = false;
		}
	}else{
		if (velx != 0 || vely != 0){
			currentState = ccs("walk");
		}else{
			currentState = ccs("idle");
		}
	}

	float posx = this->getPosition().x;
	float posy = this->getPosition().y;
	float dx = dt*(speed * speed * velx);
	float dy = dt*(speed * speed * vely);

	if(velx>0)direction = 1;
	else if(velx<0) direction=2;
	else if(vely>0) direction=3;
	else if(vely<0) direction=0;
	
	//Map Colisions
	int tx = (posx)/kTileWidth;
	int ty = (posy)/kTileHeight;
	int tdx = (posx+dx)/kTileWidth;
	int tdy = (posy+dy)/kTileHeight;
	//Horizontal
	
	if(velx!=0){
		if(!BattleLayer::PassableAt(tdx, ty) || tdx >= kMapWidth || tdx <= 0){
			velx=0;	dx=0;
		}
	}
	//Vertical
	if(vely!=0){
		if(!BattleLayer::PassableAt(tx, tdy) || tdy >= kMapHeight || tdy <= 0){
			vely=0;	dy=0;
		}
	}
	
	//Character Collision
	//First, with hero:
	
	if(this != BattleLayer::Hero()){
		CCRect heroRect = BattleLayer::Hero()->sprite->boundingBox();
		heroRect.size.width=kCharWidth;
		heroRect.size.height=kCharHeight;
		heroRect.origin.x+=kSpriteSize/2;
		heroRect.origin.y+=kSpriteSize/2;
		CCRect myRect = this->sprite->boundingBox();
		myRect.size.width=kCharWidth;
		myRect.size.height=kCharHeight;
		myRect.origin.x+=kSpriteSize/2;
		myRect.origin.y+=kSpriteSize/2;
		
		myRect.origin.x += dx;
		myRect.origin.y += dy;
		
		static float nextAttackIn=0;
		if(CCRect::CCRectIntersectsRect(heroRect, myRect)){
			int chanceToAttack = rand() % 255;
			if(chanceToAttack <= strength && nextAttackIn <= 0){
				Attack();
				nextAttackIn=3;
			}
			if(isAttacking){
				BattleLayer::Hero()->ReceiveDamageFrom(this);
			}
			velx=0;dx=0;vely=0;dy=0;
		}
		nextAttackIn -= dt;
	}
	
	//Now, with every enemy:
	for(int e=0; e < BattleLayer::Enemies()->count(); e++){
		if(this != BattleLayer::Enemies()->objectAtIndex(e)){
			CCRect otherRect = ((BaseCharacter*)BattleLayer::Enemies()->objectAtIndex(e))->sprite->boundingBox();
			otherRect.size.width=kCharWidth;
			otherRect.size.height=kCharHeight;
			otherRect.origin.x+=kSpriteSize/2;
			otherRect.origin.y+=kSpriteSize/2;
			CCRect myRect = this->sprite->boundingBox();
			myRect.size.width=kCharWidth;
			myRect.size.height=kCharHeight;
			myRect.origin.x+=kSpriteSize/2;
			myRect.origin.y+=kSpriteSize/2;
			
			myRect.origin.x += dx;
			myRect.origin.y += dy;
			
			if(CCRect::CCRectIntersectsRect(otherRect, myRect)){
				velx=0;dx=0;vely=0;dy=0;
				if(isAttacking){
					((BaseCharacter*)BattleLayer::Enemies()->objectAtIndex(e))->ReceiveDamageFrom(this);
				}
			}
		}
	}

	if(!isAttacking && !isTakingDamage){
		posx += dx;
		posy += dy;
		setPosition(ccp(posx, posy));
	}

	CCArray* state = (CCArray*)states->objectForKey(currentState->m_sString);
	int framex = ((CCInteger*)state->objectAtIndex(0))->getValue() + floor(animationStep);
	int framey = ((CCInteger*)state->objectAtIndex(1))->getValue() + direction;
	sprite->setTextureRect(getRectFor(framex, framey));

	animationStep+=(dt*speed);
	if(floor(animationStep) >= ((CCInteger*)state->objectAtIndex(2))->getValue()){
	   animationStep = 0;
	}
	
	sprite->setPosition(this->getPosition());
}

CCRect BaseCharacter::getRectFor(int x, int y) {
	return CCRect(x * kSpriteSize, y * kSpriteSize, kSpriteSize, kSpriteSize);
}

void BaseCharacter::addState(CCString* name, int x = 0, int y = 0, int asize = 0){
	CCInteger* vx = new CCInteger(x);
	CCInteger* vy = new CCInteger(y);
	CCInteger* vsize = new CCInteger(asize);

	CCArray* values = CCArray::arrayWithObjects(vx, vy, vsize, NULL);
	states->setObject(values, name->m_sString);
}

void BaseCharacter::Attack(){
	if(isAttacking || isTakingDamage) return;
	isAttacking = true;
	attackTimer = kAttackTime;
}

void BaseCharacter::removeSpriteFromParent(){
	this->m_bIsVisible = false;
	this->unscheduleAllSelectors();
}

void BaseCharacter::ReceiveDamageFrom(BaseCharacter* character){
	if(isTakingDamage)return;
	
	float damage = ((character->strength * 4) - (this->strength * 2));
	this->life -= damage;
	isTakingDamage = true;
	damageTimer = kDamageTime;
	
	if(life <= 0){
		if(this != BattleLayer::Hero()){
			BattleLayer::Enemies()->removeObject(this);
			BattleLayer::AddScore(strength);
		}
		
		sprite->runAction(CCSequence::actions(
			CCFadeTo().actionWithDuration((ccTime)0.3, 0.0),
			CCCallFunc().actionWithTarget(this, callfunc_selector(BaseCharacter::removeSpriteFromParent)),
			NULL
			));

	}
	
}